void write_xyz_file (string name, int &nPart, double &density, double &Lx, double &Ly, double &temperature, vector <state> &coordinates, int frame, double distance, bool init)
{
	if (init) {ofstream fileOutput(name.c_str(), ios_base::trunc);fileOutput.close();}
	ofstream fileOutput(name.c_str(), ios_base::app);
	fileOutput << nPart*4 << endl;
	fileOutput << "Lattice=\"" << Lx << " 0 0 0 " << Ly << " 0 0 0 1\"" << endl;
	double dn2 = 3.536/2.0;

	for(int i = 0; i < nPart; i++)
		{
			string element = "N ";
			if (abs(coordinates[i].ex_field_coeff.energy) < 1e-5) {element = "N ";}
			else if (abs(coordinates[i].ex_field_coeff.energy) < abs(u_m)*0.98) {element = "O ";}
			else {element = "C ";}

			double dop = coordinates[i].x + dn2 * coordinates[i].cos_phi;
			if (dop > Lx) {dop -= Lx;}
			if (dop < 0)  {dop += Lx;}
			double xxx = dop;
			dop = coordinates[i].y + dn2 * coordinates[i].sin_phi;
			if (dop > Ly) {dop -= Ly;}
			if (dop < 0)  {dop += Ly;}
			double yyy = dop;
			fileOutput << element << xxx << " " << yyy << " " << 0 << endl;

			dop = coordinates[i].x + dn2 * (dop_cos_angles[0] * coordinates[i].cos_phi + dop_sin_angles[0] * coordinates[i].sin_phi);
			if (dop > Lx) {dop -= Lx;}
			if (dop < 0)  {dop += Lx;}
			xxx = dop;
			dop = coordinates[i].y + dn2 * (dop_cos_angles[0] * coordinates[i].sin_phi - dop_sin_angles[0] * coordinates[i].cos_phi);
			if (dop > Ly) {dop -= Ly;}
			if (dop < 0)  {dop += Ly;}
			yyy = dop;
			fileOutput << element << xxx << " " << yyy << " " << 0 << endl;

			dop = coordinates[i].x + dn2 * (dop_cos_angles[total_molecule_directions - 2] * coordinates[i].cos_phi + dop_sin_angles[total_molecule_directions - 2] * coordinates[i].sin_phi);
			if (dop > Lx) {dop -= Lx;}
			if (dop < 0)  {dop += Lx;}
			xxx = dop;
			dop = coordinates[i].y + dn2 * (dop_cos_angles[total_molecule_directions - 2] * coordinates[i].sin_phi - dop_sin_angles[total_molecule_directions - 2] * coordinates[i].cos_phi);
			if (dop > Ly) {dop -= Ly;}
			if (dop < 0)  {dop += Ly;}
			yyy = dop;
			fileOutput << element << xxx << " " << yyy << " " << 0 << endl;

			fileOutput << "H " << coordinates[i].x << " " << coordinates[i].y << " " << 0 << endl;
		}
	fileOutput.close();
}
